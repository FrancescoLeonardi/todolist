package com.lf.todolist;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Environment;

import java.io.File;

public class Constant {
    final static String TB_NOTE = "notes";
    final static String TB_IMAGE = "images";
    final static String TB_VOCAL = "vocal";

    final static String ONLY_IMAGE = "ONLY_IMAGE";
    final static String ONLY_VOCAL = "ONLY_VOCAL";

    final static String USER_NAME = "USER_NAME";

    final static String VOCAL_PATH_FOLDER =  Environment.getExternalStorageDirectory().getAbsolutePath() + "/ToDoListVocal/";

    public static void showMessageOK(Context con, String message) {
        new AlertDialog.Builder(con)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create()
                .show();
    }

    public static void deleteVocalFile(Context con, String id){
        File file = new File(Constant.VOCAL_PATH_FOLDER, id);
        boolean deleted = file.delete();
        if(!deleted)
            showMessageOK(con, "Fail delete file from internal storage");
    }
}
