package com.lf.todolist;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.todolist.model.Note;

import java.io.File;
import java.util.ArrayList;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.lf.todolist.Constant.deleteVocalFile;
import static com.lf.todolist.Constant.showMessageOK;


public class MainActivity extends AppCompatActivity {


    private Button btnAddNote, btnMaps, btnCloud, btnMore, btnBack, btnNext;
    private ImageView btnModify, btnDelete;
    private ImageView imgCloud, imgAddNote, imgMaps;
    private TextView txtNote, txtNumberNote;
    private ProgressBar progressBar;

    private boolean log;
    private int selectedNumberNote;

    private Note selectedNote;

    private FirebaseAuth auth;
    private FirebaseFirestore db;
    private Intent toThis, toAddNote, toMore, toMaps, toCloud, toEditNote;

    private ArrayList<Note> notes;

    Handler handler;

    String nameUser;
    boolean success = true; //FOLDER


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log = false;
        handler = new Handler();
        notes = new ArrayList<>();

        toThis = new Intent(this, MainActivity.class);
        toAddNote = new Intent(this, AddNoteActivity.class);
        toEditNote = new Intent(this, AddNoteActivity.class);
        toMore = new Intent(this, ViewActivity.class);
        toMaps = new Intent(this, MapsActivity.class);
        toCloud = new Intent(this, LoginActivity.class);

        UIObject();

        permissions.add(RECORD_AUDIO);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);
        permissions.add(CAMERA);
        checkPermission();

        buttonEvent();

        try {
            FirebaseApp.initializeApp(this);

            auth = FirebaseAuth.getInstance();
            progressBar.setVisibility(View.GONE);

        }
        catch (NoSuchFieldError ex){
            showMessageOK(MainActivity.this, "Something went wrong");
        }


        progressBar.setVisibility(View.VISIBLE);


        File folder = new File(Constant.VOCAL_PATH_FOLDER);


        if (!folder.exists()) {
            success = folder.mkdirs();

            if (!success) {
                    showMessageCloseApp("Something went wrong with create Folder. Please re-open the app",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
            }
        }

        db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);


        if (auth.getCurrentUser() != null) {
            logged(true);
        }
        else{
            logged(false);
        }

        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                loadData();
            }
        });

    }

    private void showMessageCloseApp(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("Close app", okListener)
                .create()
                .show();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void UIObject(){

        btnAddNote = findViewById(R.id.btnAddNote);
        btnMaps = findViewById(R.id.btnMaps);
        btnBack = findViewById(R.id.btnBack);
        btnNext = findViewById(R.id.btnNext);
        btnCloud = findViewById(R.id.btnCloud);
        btnMore = findViewById(R.id.btnMore);
        btnModify = findViewById(R.id.btnModify);
        btnDelete = findViewById(R.id.btnDelete);
        imgCloud = findViewById(R.id.imgCloud);
        imgAddNote = findViewById(R.id.imageView);
        imgAddNote.setImageResource(R.drawable.add_note);
        imgMaps = findViewById(R.id.imageView2);
        imgMaps.setImageResource(R.drawable.maps);
        progressBar = findViewById(R.id.progressBar);
        txtNote = findViewById(R.id.txtNote);
        txtNumberNote = findViewById(R.id.txtNumberNote);

        btnBack.setEnabled(false);
        btnNext.setEnabled(false);
    }

    private void loadData(){
        notes.clear();
        db.collection(Constant.TB_NOTE)
                .whereEqualTo("tagged", nameUser)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                Note n = document.toObject(Note.class);
                                notes.add(n);
                            }

                            if(notes.size() > 0){

                                selectedNumberNote = 0;
                                selectedNote = notes.get(0);
                                setTextNote(selectedNote);

                                btnBack.setEnabled(true);
                                btnNext.setEnabled(true);
                                btnBack.setVisibility(View.VISIBLE);
                                btnNext.setVisibility(View.VISIBLE);
                                btnMore.setVisibility(View.VISIBLE);
                                btnModify.setVisibility(View.VISIBLE);
                                btnDelete.setVisibility(View.VISIBLE);
                            }
                            else{
                                btnBack.setVisibility(View.GONE);
                                btnNext.setVisibility(View.GONE);
                                btnMore.setVisibility(View.GONE);
                                btnModify.setVisibility(View.GONE);
                                btnDelete.setVisibility(View.GONE);
                                txtNote.setText("No notes");
                            }
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }

    private void buttonEvent(){

        btnCloud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(log){
                    auth.signOut();
                    FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
                        @Override
                        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) { }
                    };
                    logged(false);
                    finish();
                    startActivity(toThis);
                }
                else{
                    finish();
                    startActivity(toCloud);
                }
            }
        });

        btnAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toAddNote.putExtra(Constant.USER_NAME, nameUser);
                startActivityForResult(toAddNote, 1);
            }
        });

        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toMaps.putExtra(Constant.USER_NAME, nameUser);
                startActivity(toMaps);
            }
        });

        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toMore.putExtra(Constant.ONLY_IMAGE, "Yes");
                toMore.putExtra(Constant.ONLY_VOCAL, "Yes");
                toMore.putExtra("IDNOTE", selectedNote.id );
                startActivity(toMore);
            }
        });

        btnModify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toEditNote.putExtra("ID", selectedNote.id);
                startActivityForResult(toEditNote, 1);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);

                int tmp = selectedNumberNote+1;

                if(tmp <= notes.size()-1) {

                    btnBack.setEnabled(true);
                    btnBack.setTextColor(Color.WHITE);

                    selectedNumberNote++;

                    selectedNote = notes.get(selectedNumberNote);

                    setTextNote(selectedNote);

                }
                else {
                    btnNext.setEnabled(false);
                    btnNext.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                }

                progressBar.setVisibility(View.GONE);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);

                int tmp = selectedNumberNote-1;

                if(tmp >= 0) {
                    btnNext.setEnabled(true);
                    btnNext.setTextColor(Color.WHITE);

                    selectedNumberNote--;

                    selectedNote = notes.get(selectedNumberNote);

                    setTextNote(selectedNote);

                    progressBar.setVisibility(View.GONE);
                }else {
                    btnBack.setEnabled(false);
                    btnBack.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorPrimary));
                }

                progressBar.setVisibility(View.GONE);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                db.collection(Constant.TB_VOCAL)
                        .whereEqualTo("idNote", selectedNote.id)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        final QueryDocumentSnapshot tmp = document;

                                        db.collection(Constant.TB_VOCAL).document(document.getId())
                                                .delete()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        deleteVocalFile(MainActivity.this, tmp.getId());
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        showMessageOK(MainActivity.this, "Failure delete");
                                                        progressBar.setVisibility(View.GONE);
                                                    }
                                                });
                                    }
                                } else {
                                    showMessageOK(MainActivity.this, "Something went wrong");
                                    progressBar.setVisibility(View.GONE);
                                }

                            }
                        });

                db.collection(Constant.TB_IMAGE)
                        .whereEqualTo("idNote", selectedNote.id)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {

                                        db.collection(Constant.TB_IMAGE).document(document.getId())
                                                .delete()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        showMessageOK(MainActivity.this, "Failure delete");
                                                        progressBar.setVisibility(View.GONE);
                                                    }
                                                });
                                    }
                                } else {
                                    showMessageOK(MainActivity.this, "Something went wrong");
                                    progressBar.setVisibility(View.GONE);
                                }

                            }
                        });

                db.collection(Constant.TB_NOTE).document(selectedNote.id)
                        .delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                loadData();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                showMessageOK(MainActivity.this, "Failure delete");
                                progressBar.setVisibility(View.GONE);
                            }
                        });
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         progressBar.setVisibility(View.VISIBLE);
         MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                loadData();
            }
         });
    }

    private void setTextNote(final Note note){
        handler.post(new Runnable(){
            public void run() {
                txtNote.setText(note.txtNote);
                txtNumberNote.setText(Integer.toString(selectedNumberNote));
            }
        });
    }

    /*
        Change the row into the main dinamically if user are in or out firebase
     */
    private void logged(boolean log_in){

        if(log_in){
            log = true;
            imgCloud.setImageResource(R.drawable.cloud_in);
            btnCloud.setText(R.string.cloud_in);
            nameUser = auth.getCurrentUser().getEmail();
        }
        else{
            log = false;
            imgCloud.setImageResource(R.drawable.cloud_out);
            btnCloud.setText(R.string.cloud_out);
            nameUser = "";
            showMessageLogInCancel("You are not login!",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(toCloud);
                        }
                    });

        }
    }

    private void showMessageLogInCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("Log me in", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    //region permessi

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    private final static int ALL_PERMISSIONS_RESULT = 101;

    public void checkPermission(){
        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    //endregion



}
