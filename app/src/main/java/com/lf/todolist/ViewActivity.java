package com.lf.todolist;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lf.todolist.model.Image;
import com.lf.todolist.model.NoteExtraObject;
import com.lf.todolist.model.Vocal;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.lf.todolist.Constant.showMessageOK;

public class ViewActivity extends AppCompatActivity {

    List<NoteExtraObject> notes;
    TextView txtNoFile;
    ListView lst;
    String idNote;

    ProgressBar progressBar;

    boolean bOnlyImage;
    boolean bOnlyVocal;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listview);

        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        bOnlyVocal = false;
        bOnlyImage = false;

        lst = findViewById(R.id.listView);

        notes = new ArrayList<>();
        txtNoFile = findViewById(R.id.txtNoFile);
        progressBar = findViewById(R.id.progressBar);

        idNote = getIntent().getStringExtra("IDNOTE");

        if(getIntent().getStringExtra(Constant.ONLY_IMAGE) != null)
            bOnlyImage = true;

        if(getIntent().getStringExtra(Constant.ONLY_VOCAL) != null)
            bOnlyVocal = true;

        ListAdapter customAdapter = new ListAdapter(this, R.layout.row_note, notes);
        lst.setAdapter(customAdapter);

        ViewActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                loadData();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();


        lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                if(notes.get(position).getType() == "IMAGE"){
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(((Image)notes.get(position)).uri), "image/*");
                    startActivity(intent);
                }

                if(notes.get(position).getType() == "VOCAL"){
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                    File audioFile = new File(((Vocal)notes.get(position)).vocalPath);
                    intent.setDataAndType(Uri.fromFile(audioFile), "audio/mp3");
                    startActivity(intent);
                }

                ImageView btnDelete = view.findViewById(R.id.imgDelete);
                btnDelete.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View arg0) {
                                progressBar.setVisibility(View.VISIBLE);
                                if(notes.get(position).getType() == "VOCAL") {
                                    db.collection(Constant.TB_VOCAL).document(notes.get(position).idDelete)
                                            .delete()
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Constant.deleteVocalFile(ViewActivity.this , notes.get(position).idDelete);
                                                    loadData();
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    showMessageOK(ViewActivity.this, "Something went wrong");
                                                    progressBar.setVisibility(View.GONE);
                                                }
                                            });
                                }
                                if(notes.get(position).getType() == "IMAGE") {
                                    db.collection(Constant.TB_IMAGE).document(notes.get(position).idDelete)
                                            .delete()
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    loadData();
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    showMessageOK(ViewActivity.this, "Something went wrong");
                                                    progressBar.setVisibility(View.GONE);
                                                }
                                            });
                                }
                            }
                        });
            }
        });

    }


    private void loadData(){
        notes.clear();

        if(bOnlyVocal){
            db.collection(Constant.TB_VOCAL)
                    .whereEqualTo("idNote", idNote)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {

                                    Map<String, Object> map = document.getData();
                                    notes.add(new Vocal(
                                                    document.getId(),
                                                    map.get("vocalPath").toString()
                                            )
                                    );
                                }

                                if(notes.size() > 0)
                                    txtNoFile.setVisibility(View.GONE);

                                ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();
                            } else {
                            }

                        }
                    });
        }



        if(bOnlyImage){
            db.collection(Constant.TB_IMAGE)
                    .whereEqualTo("idNote", idNote)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {

                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {

                                    Map<String, Object> map = document.getData();
                                    notes.add(new Image(
                                                    document.getId(),
                                                    map.get("uri").toString()
                                            )
                                    );
                                }

                                if(notes.size() > 0)
                                    txtNoFile.setVisibility(View.GONE);

                                ((ArrayAdapter) lst.getAdapter()).notifyDataSetChanged();
                            }

                        }
                    });
        }


        progressBar.setVisibility(View.GONE);
    }


    public class ListAdapter extends ArrayAdapter<NoteExtraObject> {

        private int resourceLayout;
        private Context mContext;

        public ListAdapter(Context context, int resource, List<NoteExtraObject> items) {
            super(context, resource, items);
            this.resourceLayout = resource;
            this.mContext = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(mContext);
                v = vi.inflate(resourceLayout, null);
            }

            NoteExtraObject p = getItem(position);

            if (p != null) {
                TextView txtTitle = v.findViewById(R.id.txtNote);
                ImageView imgTop = v.findViewById(R.id.imgTop);
                ImageView btnDelete = v.findViewById(R.id.imgDelete);

                txtTitle.setText(p.getType());

                if(p.getType()=="IMAGE")
                    imgTop.setImageResource(R.drawable.image);

                if(p.getType()=="VOCAL")
                    imgTop.setImageResource(R.drawable.icon_mp3);

                imgTop.setVisibility(View.VISIBLE);

            }

            return v;
        }

    }
}
