package com.lf.todolist.model;

public class Image extends NoteExtraObject {

    public String uri;

    public Image(){}

    public Image(String id, String URI){
        this.idDelete = id;
        this.uri = URI;
    }

    @Override
    public String getType() {
        return IMAGE;
    }
}
