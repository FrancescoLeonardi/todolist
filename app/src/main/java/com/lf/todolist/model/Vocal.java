package com.lf.todolist.model;

public class Vocal extends NoteExtraObject {

    public String vocalPath;

    public Vocal(){}

    public Vocal(String id, String vP){
        this.idDelete = id;
        this.vocalPath = vP;
    }

    @Override
    public String getType() {
        return VOCAL;
    }
}
