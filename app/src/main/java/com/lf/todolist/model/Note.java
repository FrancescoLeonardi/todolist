package com.lf.todolist.model;

public class Note {

    public String id;
    public String txtNote;
    public Double lat,lon;
    public String tagged;

    public Note(){}

    public Note(String id, String txtNote, double o1, double o2, String tagged){
        this.id = id;
        this.txtNote = txtNote;
        this.lat = o1;
        this.lon = o2;
        this.tagged = tagged;
    }
}
