package com.lf.todolist;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.lf.todolist.model.Image;
import com.lf.todolist.model.Note;
import com.lf.todolist.model.Vocal;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static com.lf.todolist.Constant.showMessageOK;


public class AddNoteActivity extends AppCompatActivity {

    private Button btnAddImage, btnViewImage, btnAddVocal, btnViewVocal, btnSend, btnCamera;
    private ImageView btnRecord;
    private EditText editTextNote;
    private TextView txtGps;

    private ProgressBar progressBar;
    private FirebaseFirestore db;
    private String id;

    private static final int REQ_CODE_SPEECH_INPUT = 100;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 888;
    private static final int RESULT_LOAD_IMAGE = 2;
    private static final String vocalPath =  Constant.VOCAL_PATH_FOLDER;

    double latitude, longitude;

    LocationTracker tracker;
    private Intent toView;

    String nameUser;

    ArrayList<Image> images;

    /*
        TODO: permessi per il microfono
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnote);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        images = new ArrayList<>();

        editTextNote = findViewById(R.id.edtxtNote);
        btnAddImage = findViewById(R.id.btnAddImage);
        btnViewImage = findViewById(R.id.btnViewImage);
        btnSend = findViewById(R.id.btnSend);
        btnCamera = findViewById(R.id.btnCamera);
        btnRecord = findViewById(R.id.btnRecord);
        btnAddVocal = findViewById(R.id.btnAddVocal);
        btnViewVocal = findViewById(R.id.btnViewVocal);
        progressBar = findViewById(R.id.progressBar);
        txtGps = findViewById(R.id.txtGps);

        nameUser = getIntent().getStringExtra(Constant.USER_NAME);

        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);

        buttonEvent();

        tracker=new LocationTracker(AddNoteActivity.this);
        // check if location is available
        if(tracker.isLocationEnabled)
        {
            latitude=tracker.getLatitude();
            longitude=tracker.getLongitude();
            txtGps.setText("Lat: "+latitude+" , Lon: "+longitude);
            if(latitude==0 && longitude == 0){
                latitude=tracker.getLatitude();
                longitude=tracker.getLongitude();
                txtGps.setText("Lat: "+latitude+" , Lon: "+longitude);
            }
        }
        else
        {
            try {
                tracker.askToOnLocation();
            }
            catch(Exception ex){
                showMessageOK(AddNoteActivity.this, "Error GPS");
            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        try {
            id = getIntent().getStringExtra("ID");
            if(id != null && id != "")
                AddNoteActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    loadData();

                }
            });
            else{
                DocumentReference ref = db.collection(Constant.TB_NOTE).document();
                id = ref.getId();
                progressBar.setVisibility(View.GONE);
            }


        }catch(Exception ex){
            id = "";
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        toView = new Intent(this, ViewActivity.class);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(!images.isEmpty()){
            for(Image img : images){
                img.idNote = id;
                db.collection(Constant.TB_IMAGE).document(img.idDelete).set(img);
            }
        }
    }

    private void loadData(){

        DocumentReference docRef = db.collection(Constant.TB_NOTE).document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    Map<String, Object> map = document.getData();
                    if (document.exists()) {

                        if(map.get("txtNote").toString().length() > 0)
                            editTextNote.setText(map.get("txtNote").toString());
                    } else {
                    }
                } else {
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }




    private void buttonEvent(){

        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startVoiceInput();
            }
        });


        btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });

        btnCamera.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                {
                    Constant.showMessageOK(AddNoteActivity.this, "No camera permission");
                }
                else
                {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });

        btnViewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toView.putExtra(Constant.ONLY_IMAGE , "YES");
                toView.putExtra("IDNOTE", id );
                startActivity(toView);
            }
        });

        btnAddVocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DocumentReference ref = db.collection(Constant.TB_VOCAL).document();
                String idVocal = ref.getId();
                recordAudio(idVocal);
                Vocal vocal = new Vocal();
                vocal.vocalPath = vocalPath + idVocal;
                vocal.idNote = id;
                vocal.idDelete = idVocal;
                db.collection(Constant.TB_VOCAL).document(idVocal).set(vocal);

            }
        });

        btnViewVocal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                toView.putExtra(Constant.ONLY_VOCAL , "YES");
                toView.putExtra("IDNOTE", id );
                startActivity(toView);
            }
        });


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(editTextNote.getText().toString().isEmpty())
                    editTextNote.setText("Empty field");
                if(nameUser == null)
                    nameUser = "";
                Note note = new Note(id ,editTextNote.getText().toString(), latitude, longitude, nameUser);
                db.collection(Constant.TB_NOTE).document(id).set(note);
                for(Image img : images){
                    img.idNote = note.id;
                    db.collection(Constant.TB_IMAGE).document(img.idDelete).set(img);
                }
                images.clear();
                finish();
            }
        });
    }


    private void startVoiceInput() {
        int permissionCheck = ContextCompat.checkSelfPermission(AddNoteActivity.this, Manifest.permission.RECORD_AUDIO);

        if (permissionCheck == PERMISSION_GRANTED) {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hello, How can I help you?");
            try {
                startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
            } catch (ActivityNotFoundException a) {

            }

        }else {

            showMessageOK(AddNoteActivity.this, "Permession not granted");
            ActivityCompat.requestPermissions(AddNoteActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, 0);
        }
    }


    public void recordAudio(String fileName) {
        //region mic

        final MediaRecorder recorder = new MediaRecorder();
        ContentValues values = new ContentValues(3);
        values.put(MediaStore.MediaColumns.TITLE, fileName);
        // /storage/emulated/0/2MArtTzkkgoYFy6BuvVa
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);
        recorder.setOutputFile(vocalPath + fileName);
        try {
            recorder.prepare();
        } catch (Exception e){
            e.printStackTrace();
        }

        final ProgressDialog mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("RECORDING");
        mProgressDialog.setMessage(" Recording... ");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setButton("Stop recording", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                mProgressDialog.dismiss();
                recorder.stop();
                recorder.release();
            }
        });

        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
            public void onCancel(DialogInterface p1) {
                recorder.stop();
                recorder.release();
            }
        });
        recorder.start();
        mProgressDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {

            case RESULT_LOAD_IMAGE:

                if(resultCode == RESULT_OK && null != data) {
                    final Uri imageUri = data.getData();

                    DocumentReference ref = db.collection(Constant.TB_IMAGE).document();
                    String idImage = ref.getId();
                    Image img = new Image();
                    img.uri = imageUri.toString();
                    img.idDelete = idImage;
                    images.add(img);

                }
                break;

            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    editTextNote.setText(result.get(0));
                }
                break;
            }

            case CAMERA_REQUEST:
                if(resultCode == RESULT_OK)
                {
                    Constant.showMessageOK(AddNoteActivity.this, "Image add to gallery");
                   // Bitmap photo = (Bitmap) data.getExtras().get("data");
                   // imageView.setImageBitmap(photo);
                }
                break;

        }
    }





}
